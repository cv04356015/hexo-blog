title: Anaconda 安裝建置
author: Hsien Ching Lo
date: 2018-03-31 11:15:55
tags:
---
# How To Install the Anaconda Python Distribution on Ubuntu 16.04

---

## Installing Anaconda

from: [https://www.digitalocean.com/community/tutorials/how-to-install-the-anaconda-python-distribution-on-ubuntu-16-04](https://www.digitalocean.com/community/tutorials/how-to-install-the-anaconda-python-distribution-on-ubuntu-16-04)

## 由於多人使用  ... 更改路徑為 /opt/anaconda3

The installation process will continue, it may take some time.

Once it’s complete you’ll receive the following output:

```
Output
...
installation finished.
Do you wish the installer to prepend the Anaconda3 install location
to PATH in your /home/sammy/.bashrc ? [yes|no]
[no] 
>
>
>
```

Type`yes`so that you can use the`conda`command. You’ll next see the following output:

```
Output
Prepending PATH=/home/sammy/anaconda3/bin to PATH in /home/sammy/.bashrc
A backup will be made to: /home/sammy/.bashrc-anaconda3.bak
...
```

In order to activate the installation, you should source the`~/.bashrc`file:

```

```

Once you have done that, you can verify your install by making use of the`conda`command, for example with`list`:

```

```

You’ll receive output of all the packages you have available through the Anaconda installation:

```
Output
# packages in environment at /home/sammy/anaconda3:
#
_ipyw_jlab_nb_ext_conf    0.1.0            py36he11e457_0  
alabaster                 0.7.10           py36h306e16b_0  
anaconda                  5.0.1            py36hd30a520_1
...
```

Now that Anaconda is installed, we can go on to setting up Anaconda environments.

## Setting Up Anaconda Environments {#setting-up-anaconda-environments}

Anaconda virtual environments allow you to keep projects organized by Python versions and packages needed. For each Anaconda environment you set up, you can specify which version of Python to use and can keep all of your related programming files together within that directory.

First, we can check to see which versions of Python are available for us to use:

```

```

You’ll receive output with the different versions of Python that you can target, including both Python 3 and Python 2 versions. Since we are using the Anaconda with Python 3 in this tutorial, you will have access only to the Python 3 versions of packages.

Let’s create an environment using the most recent version of Python 3. We can achieve this by assigning version 3 to the`python`argument. We’ll call the environmentmy\_env, but you’ll likely want to use a more descriptive name for your environment especially if you are using environments to access more than one version of Python.

```

```

We’ll receive output with information about what is downloaded and which packages will be installed, and then be prompted to proceed with`y`or`n`. As long as you agree, type`y`.

The`conda`utility will now fetch the packages for the environment and let you know when it’s complete.

You can activate your new environment by typing the following:

```

```

With your environment activated, your command prompt prefix will change:

```

```

Within the environment, you can verify that you’re using the version of Python that you had intended to use:

```

```

```
Output
Python 3.6.0 :: Continuum Analytics, Inc.
```

When you’re ready to deactivate your Anaconda environment, you can do so by typing:

```

```

Note that you can replace the word`source`with`.`to achieve the same results.

To target a more specific version of Python, you can pass a specific version to the`python`argument, like`3.5`, for example:

```

```

You can update your version of Python along the same branch \(as in updating Python 3.5.1 to Python 3.5.2\) within a respective environment with the following command:

```

```

If you would like to target a more specific version of Python, you can pass that to the`python`argument, as in`python=3.3.2`.

You can inspect all of the environments you have set up with this command:

```

```

```
Output
# conda environments:
#
my_env                   /home/sammy/anaconda3/envs/my_env
my_env35                 /home/sammy/anaconda3/envs/my_env35
root                  *  /home/sammy/anaconda3
```

The asterisk indicates the current active environment.

Each environment you create with`conda create`will come with several default packages:

* `openssl`
* `pip`
* `python`
* `readline`
* `setuptools`
* `sqlite`
* `tk`
* `wheel`
* `xz`
* `zlib`

You can add additional packages, such as`numpy`for example, with the following command:

```

```

If you know you would like a`numpy`environment upon creation, you can target it in your`conda create`command:

```

```

If you are no longer working on a specific project and have no further need for the associated environment, you can remove it. To do so, type the following:

```

```

Now, when you type the`conda info --envs`command, the environment that you removed will no longer be listed.

## Updating Anaconda {#updating-anaconda}

You should regularly ensure that Anaconda is up-to-date so that you are working with all the latest package releases.

To do this, you should first update the`conda`utility:

```

```

When prompted to do so, type`y`to proceed with the update.

Once the update of`conda`is complete, you can update the Anaconda distribution:

```

```

Again when prompted to do so, type`y`to proceed.

This will ensure that you are using the latest releases of`conda`and Anaconda.

## Uninstalling Anaconda {#uninstalling-anaconda}

If you are no longer using Anaconda and find that you need to uninstall it, you should start with the`anaconda-clean`module which will remove configuration files for when you uninstall Anaconda.

```

```

Type`y`when prompted to do so.

Once it is installed, you can run the following command. You will be prompted to answer`y`before deleting each one. If you would prefer not to be prompted, add`--yes`to the end of your command:

```
anaconda-clean
```

This will also create a backup folder called`.anaconda_backup`in your home directory:

```
Output
Backup directory: /home/
sammy
/.anaconda_backup/
2017-01-25T191831
```

You can now remove your entire Anaconda directory by entering the following command:

```

```

Finally, you can remove the PATH line from your`.bashrc`file that Anaconda added. To do so, first open nano:

```

```

Then scroll down to the end of the file \(if this is a recent install\) or type`CTRL + W`to search for Anaconda. Delete or comment out the following lines:

/home/sammy/.bashrc

```
# added by Anaconda3 4.2.0 installer
export PATH="/home/sammy/anaconda3/bin:$PATH"
```

When you’re done editing the file, type`CTRL + X`to exit and`y`to save changes.

Anaconda is now removed from your server.

## Conclusion {#conclusion}

This tutorial walked you through the installation of Anaconda, working with the`conda`command-line utility, setting up environments, updating Anaconda, and deleting Anaconda if you no longer need it.

You can use Anaconda to help you manage workloads for data science, scientific computing, analytics, and large-scale data processing.

# Anaconda add path 

```
sudo su
vim /etc/environment
```

add anaconda path 在原先的 path 之前

```
/opt/anaconda3/bin/:
```

```
PATH="/opt/anaconda3/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
```

套用變數

```
source /etc/environment
```

# Uninstalling Anaconda

---

from :[https://docs.anaconda.com/anaconda/install/uninstall](#)

from:[https://stackoverflow.com/questions/22585235/python-anaconda-how-to-safely-uninstall](#)



To uninstall Anaconda, you can do a simple remove of the program. This will leave a few files behind, which for most users is just fine. See Option A.

If you also want to remove all traces of the configuration files and directories from Anaconda and its programs, you can download and use the Anaconda-Clean program first, then do a simple remove. See Option B.

1. Option A. Use simple remove to uninstall Anaconda:

   * Windows–In the Control Panel, choose Add or Remove Programs or Uninstall a program, and then select Python 3.6 \(Anaconda\) or your version of Python.
   * macOS–Open the Terminal.app or iTerm2 terminal application, and then remove your entire Anaconda directory, which has a name such as
     `anaconda2`
     or
     `anaconda3`
     , by entering
     `rm`
     `-rf`
     `~/anaconda3`
     .
   * Linux–Open a terminal window, and then remove your entire Anaconda directory, which has a name such as
     `anaconda2`
     or
     `anaconda3`
     , by entering
     `rm`
     `-rf`
     `~/anaconda3`
     .

2. Option B. Full uninstall using Anaconda-Clean and simple remove.

   NOTE: Anaconda-Clean must be run before simple remove.

   * Install the Anaconda-Clean package from Anaconda Prompt or a terminal window:

     ```
     conda
     install
     anaconda
     -
     clean
     ```

   * In the same window, run one of these commands:

     * Remove all Anaconda-related files and directories with a confirmation prompt before deleting each one:

       ```
       anaconda
       -
       clean
       ```

     * Or, remove all Anaconda-related files and directories without being prompted to delete each one:

       ```
       anaconda
       -
       clean
       --
       yes
       ```

     Anaconda-Clean creates a backup of all files and directories that might be removed, such as`.bash_profile`, in a folder named`.anaconda_backup`in your home directory. Also note that`Anaconda-Clean`leaves your data files in the AnacondaProjects directory untouched.

   * After using Anaconda-Clean, follow the instructions above in Option A to uninstall Anaconda.

## Removing Anaconda path from .bash\_profile

If you use Linux or macOS, you may also wish to check the`.bash_profile`file in your home directory for a line such as:

```
exportPATH="/Users/jsmith/anaconda3/bin:$PATH"
```

NOTE: Replace`/Users/jsmith/anaconda3/`with your actual path.

This line adds the Anaconda path to the PATH environment variable. It may refer to either Anaconda or Miniconda. After uninstalling Anaconda, you may delete this line and save the file.

