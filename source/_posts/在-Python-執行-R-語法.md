title: 在 Python 執行 R 語法
author: Hsien Ching Lo
tags:
  - Python
categories: []
date: 2018-03-31 11:06:00
---
# python run R

```
import ipdb

import os
import pyodbc
import pandas as pd
import rpy2
os.environ['NLS_LANG'] = 'AMERICAN_AMERICA.ZHT16MSWIN950'
server = '192.168.170.54'
database = 'CDCDW'
username = 'sas'
password = 'ueCr5brAD6u4rAs62t9a'
cnxn = pyodbc.connect('DRIVER={Oracle};SERVER='+server+';PORT=1561;DATABASE='+database+';UID='+username+';PWD='+ password)
tsql = "SELECT * FROM CDCDW.V_MORNING_MEETING_REPORT_ALL WHERE ROWNUM < 5;"
df = pd.read_sql_query(tsql, cnxn)
get_ipython().magic('load_ext rpy2.ipython')
get_ipython().magic('R -i df')
get_ipython().run_cell_magic('R', '','library(dplyr)\ndf2 <- df %>%\n    filter(NAME == "陳奕潔")\nwrite.csv(df2, "df2.csv")\n')
```