title: R 資料處理
author: Hsien Ching Lo
tags:
  - R
categories: []
date: 2018-03-31 13:47:00
---
# 字串處理

---

計算文字在句子中出現次數

stringr::str_count

```
library(stringr)
str_count("我我我我我", "我")
str_count(c("我我我我我", "我我"), "我")
```

# 將 table 轉換成 data.frame 示例

table is.

```
table(df$DISEASE_CATEGOR, df$DISTRICT)
```

```     
         中區 北區 台北區 東區 南區 高屏區
  其他      2    0      4    0    2      0
  第二類    4    3     10    0    1     11
  第三類   23   22     54    4   18     23
  第四類    2    4      8    5    1     14
```

dataframe is.

```
table(df$DISEASE_CATEGOR, df$DISTRICT) %>% 
  as.data.frame.matrix()
```

```
       中區 北區 台北區 東區 南區 高屏區
其他      2    0      4    0    2      0
第二類    4    3     10    0    1     11
第三類   23   22     54    4   18     23
第四類    2    4      8    5    1     14
```

# 日期格式

## lubridate

https://zhuanlan.zhihu.com/p/27612862


# 使用 parallel 多執行緒運算

透過並行處理達到以更少的時間完成排程

* lapply() 和 sapply() 函數 (非多執行緒，但寫法多參考此方法)
* parallel package
* doParallel package
* snow package


## lapply() 和 sapply() 函數

lapply() 和 sapply() 函數的計算速度非常快。這意味著這些值是彼此獨立計算的。但是，它在執行過程中並不平行。R中有各種允許並行化的軟件包。

```
library(parallel)
# Calculate the number of cores
no_cores <- detectCores() - 1
# Initiate cluster
cl <- makeCluster(no_cores)
parLapply(cl, 2:4, function(exponent) 2^exponent)
```

## parallel package

一般 parallel 程序

```
#Include the parallel library. If the next line does not work, run install.packages(“parallel”) first
library(parallel)
  
# Use the detectCores() function to find the number of cores in system
no_cores <- detectCores()
  
# Setup cluster
clust <- makeCluster(no_cores) #This line will take time

# The parallel version of lapply() is parLapply() and needs an additional cluster argument.
parLapply(clust,1:5, function(x) c(x^2,x^3))

# Stop Cluster
stopCluster(clust)
```

cluster 下的變數與套件是獨立於原本的 session，因此需要透過 clusterExport 匯入，否則 cluster 下會找不到此變數與套件。

```
#Include the parallel library. If the next line does not work, run install.packages(“parallel”) first
library(parallel)
 
# Use the detectCores() function to find the number of cores in system
no_cores <- detectCores()
 
# Setup cluster
clust <- makeCluster(no_cores) #This line will take time
 
# Setting a base variable 
base <- 4

# Note that this line is required so that all cores in cluster have this variable available
clusterExport(clust, "base")

# Using the parSapply() function
parSapply(clust, 1:5, function(exponent) base^exponent)
 
# Stop Cluster
stopCluster(clust)
```

套件讀取實例

```
clusterEvalQ(clust, library(randomForest))
```

## doParallel package

[foreach](https://www.rdocumentation.org/packages/foreach/versions/1.4.4/topics/foreach) 
[Introduce to foreach and iterators](http://chingchuan-chen.github.io/posts/201704/2017-04-23-introduce-to-foreach-and-iterators.html)
[Implementing Parallel Processing in R](https://www.r-bloggers.com/implementing-parallel-processing-in-r/)

* %do% evaluates the expression sequentially
* %dopar% evalutes it in parallel. 
* .combine 的預設是 list 因此會產出一個 list，另有其他輸出方式

```
library(doParallel)
registerDoParallel(makeCluster(no_cores))

# Vector output
foreach(exponent = 1:5, .combine = c) %dopar% base^exponent

# Matrix output
foreach(exponent = 1:5, .combine = rbind) %dopar% base^exponent

# List output
foreach(exponent = 1:5, .combine = list, .multicombine=TRUE) %dopar%  base^exponent

# Data Frame output
foreach(exponent = 1:5, .combine = data.frame) %dopar% base^exponent
```