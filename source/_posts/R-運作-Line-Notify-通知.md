title: R 運作 Line Notify 通知
author: Hsien Ching Lo
date: 2018-03-20 22:53:21
tags:
---
傳送訊息

```
library(httr)

msg <- list(
  message = "Hello World"
)

httr::POST(
  "https://notify-api.line.me/api/notify/",
  add_headers(Authorization = "Bearer [token]"),
  content_type("application/x-www-form-urlencoded"),
  body = msg,
  encode = "form"
) 
```