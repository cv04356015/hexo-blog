title: SAS Viya Notes
author: Hsien Ching Lo
date: 2018-05-18 01:21:24
tags:
---
Christine
Student1

# LDAP Admin

abbott.ldif

```
dn: uid=abbott,ou=users,dc=demo,dc=com
objectClass: inetOrgPerson
objectClass: organizationalPerson
objectClass: top
cn: Abbott
sn: abbott
displayName: Abbott
employeeNumber: P299
l: Cary
mail: Abbott@demo.com
o: Orion Star
title: Data Administrator
uid: abbott
userPassword:: U3R1ZGVudDE=
```

## init sas-admin & login

```
cd /opt/sas/viya/home/bin/
$ ./sas-admin profile init
```

```
Service Endpoint> http://server

Output type (text|json|fulljson)> text

Enable ANSI colored output (y/n)?> n
```

```
$ ./sas-admin auth login
```

## sas-admin help

```
$ ./sas-admin help
```

## list-users

```
$ ./sas-admin identities list-users
```

```
Id          Name        Description   State
Susan       Susan       <nil>         active
ahmed       Ahmed       <nil>         active
anita       Anita       <nil>         active
barbara     Barbara     <nil>         active
bruno       Bruno       <nil>         active
cas         cas         <nil>         active
christine   Christine   <nil>         active
ellen       Ellen       <nil>         active
eric        Eric        <nil>         active
george      George      <nil>         active
gloria      Gloria      <nil>         active
harvey      Harvey      <nil>         active
henri       Henri       <nil>         active
jacques     Jacques     <nil>         active
kari        Kari        <nil>         active
linda       Linda       <nil>         active
lynn        Lynn        <nil>         active
marcel      Marcel      <nil>         active
mark        Mark        <nil>         active
ole         Ole         <nil>         active
robert      Robert      <nil>         active
sally       Sally       <nil>         active
samantha    Samantha    <nil>         active
sasadm      sasadm      <nil>         active
sasldap     sasldap     <nil>         active
stephanie   Stephanie   <nil>         active
student     Student     <nil>         active
```

## license check

```
$ ./sas-admin licenses count
```

```
Specifies the number of products that are licensed: 79
```

```
$ ./sas-admin licenses site-info list
```

```
Site Name                               EDU_VA82_17w47
Site Number                             70180938
Operating System Name                   LIN X64
Release                                 V03
Server Date                             2018-05-17
Server Date (numeric)                   21321
Grace Period (days)                     45
Warning Period (days)                   55
```

# SAS Studio

## add 3 session

```
cas mySession1 sessopts=(caslib=casuser timeout=1800 locale="en_US");
cas mySession2 sessopts=(caslib=casuser timeout=1800 locale="en_US");
cas mySession3 sessopts=(caslib=casuser timeout=1800 locale="en_US");
```

## manager the cas session

* SAS Environment Manager
* cmd


SAS Environment Manager -> Data -> Value(Servers)


```
$ ./sas-admin -k cas sessions list -server cas-shared-default --superuser --owner eric
```

check a user session status

```
s-shared-default --superuser --owner eric
Name                                  Session ID                                                                                                                     Owner   State       Authentication Type
MYSESSION3:Thu May 17 22:07:59 2018   b6055283-c5c2-a14a-8da3-d78e5                                                                                        8508d99   eric    Connected   OAuth/External PAM
```

# audit report

![](/blog/images/pasted-10.png)

編輯

![](/blog/images/pasted-9.png)

可以修改 audit report 保留天數 7d -> 28d

![](/blog/images/pasted-8.png)


# 查看服務啟動狀態

```
$ sudo service sas-viya-all-services status
```

```
Getting service info from consul...
  Service                                            Status     Host               Port     PID
  sas-viya-consul-default                            up         N/A                 N/A    4858
  sas-viya-sasdatasvrc-postgres-node0-ct-pg_hba      up         N/A                 N/A    7489
  sas-viya-sasdatasvrc-postgres-node0-ct-postgresql  up         N/A                 N/A    7498
  sas-viya-sasdatasvrc-postgres-pgpool0-ct-pcp       up         N/A                 N/A    7479
  sas-viya-sasdatasvrc-postgres-pgpool0-ct-pgpool    up         N/A                 N/A    7515
  sas-viya-sasdatasvrc-postgres-pgpool0-ct-pool_hba  up         N/A                 N/A    7527
  sas-viya-vault-default                             up         10.250.27.169      8200     N/A
  sas-viya-sasdatasvrc-postgres-node0                up         N/A                 N/A   12666
  sas-viya-cascontroller-default                     up         N/A                 N/A   19041
  sas-viya-connect-default                           up         N/A                 N/A    9661
  sas-viya-httpproxy-default                         up         N/A                 N/A    9577
  sas-viya-rabbitmq-server-default                   up         10.250.27.169      5672    None
  sas-viya-sasdatasvrc-postgres                      up         N/A                 N/A   12939
```

service operating

```
$ sudo service sas-viya-backup-agent-default status
$ sudo service sas-viya-backup-agent-default stop
$ sudo service sas-viya-backup-agent-default start
```

# Backup and Recovery

## 建立修改目錄並備份(環境設定)

```
$ cd /workshop/
$ mkdir BACKUP
$ sudo chmod -R 777 BACKUP/
```

![upload successful](/blog/images/pasted-12.png)

![upload successful](/blog/images/pasted-13.png)

## check backup info

會看到剛剛建立的 backup 資訊

```
$ cd /opt/sas/viya/home/bin/
$ ./sas-admin auth login
$ ./sas-admin backup list
```

# data upload


```
/*****************************************************************************/
/*  Create a default CAS session and create SAS librefs for existing caslibs */
/*  so that they are visible in the SAS Studio Libraries tree.               */
/*****************************************************************************/

cas; 
caslib _all_ assign;
cas tw_session sessopts=(caslib=casuser timeout=99 locale='en_US' messagelevel=all);

data work.new;
tday = today(); output;
tday = today()-1; output;
tday = today()-2; output;
run;

proc casutil;
load data=work.new casout='cas_new' outcaslib='HQDATA' promote;
save casdata='cas_new' incaslib='HQDATA' casout='cas_new_hqdata' outcaslib='HQDATA' replace;
list files incaslib='HQDATA';
run;quit;
cas tw_session terminate;
```


# CAS server 匯入資料大小限制修改

configuration -> All services -> CAS Management service -> sas.casmanagement -> maxFileUploadSize

![upload successful](/blog/images/pasted-14.png)


# load data to CAS server (caslib)

```
/*****************************************************************************/
/*  Create a default CAS session and create SAS librefs for existing caslibs */
/*  so that they are visible in the SAS Studio Libraries tree.               */
/*****************************************************************************/

cas; 
caslib _all_ assign;

/*****************************************************************************/
/*  Load SAS data set from a Base engine library (library.tableName) into    */
/*  the specified caslib ("myCaslib") and save as "targetTableName".         */
/*****************************************************************************/

proc casutil;
	load data=sashelp.cars outcaslib="casuser"
	casout="carsCAS";
run;
```

# create CAS Libraries & load data on CLI

```
cd /opt/sas/viya/home/bin
./sas-admin cas caslibs create path --caslib Finance --path /Fianace --server cas-shared-default --description "Finance data"
./sas-admin cas caslibs create path --caslib TPDATA --path /TW/TP/DATA --server cas-shared-default --description "TP data"
```

![](/blog/images/pasted-15.png)

檢查內容

```
./sas-admin -output text cas caslibs list -server cas-shared-default
```

## load data 

```
./sas-admin cas tables load --caslib=Workshop --table=EMPLOYEES --server cas-shared-default
./sas-admin cas tables load --caslib=Workshop --table=* --server cas-shared-default
```

## session caslibs & global caslibs

一般使用者無法上傳 global caslibs 資料

![](/blog/images/pasted-16.png)

```
CAS mySession SESSOPTS=(CASLIB=casuser timeout=99 locale='en_US');

CASLIB myCaslib PATH="/tmp" TYPE=path;
PROC CASUTIL;
LOAD DATA=sashelp.air OUTCASLIB="MYCASLIB" CASOUT="air";
RUN;

CASLIB _ALL_ ASSIGN;
proc casutil;
list tables incaslib="MYCASLIB";
run;

/*
lynn can't promote tables to global caslibs
ERROR: The caslib 'MYCASLIB' is a session caslib. You cannot promote tables to global scope in session caslibs.
ERROR: The action stopped due to errors.
*/

proc casutil;
list tables outcaslib="MYCASLIB";
promote casdata="air";
run;quit;
```