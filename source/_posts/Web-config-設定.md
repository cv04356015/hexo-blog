title: Web.config 設定
author: Hsien Ching Lo
tags:
  - ASP.NET
categories: []
date: 2018-03-31 14:32:00
---
# 設定所有檔案皆可下載

```
<?xml version="1.0" encoding="UTF-8"?>
 <configuration>
     <system.webServer>
         <staticContent>
             <mimeMap fileExtension="." mimeType="application/octet-stream" />
         </staticContent>
     </system.webServer>
 </configuration>
 ```