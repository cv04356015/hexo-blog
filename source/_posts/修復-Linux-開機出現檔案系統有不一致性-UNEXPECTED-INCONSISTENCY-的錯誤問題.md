title: 修復 Linux 開機出現檔案系統有不一致性 (UNEXPECTED INCONSISTENCY) 的錯誤問題
author: Hsien Ching Lo
tags:
  - linux
categories: []
date: 2018-06-17 00:27:00
---
Linux 無法正常開機，出現錯誤訊息:

```
Checking filesystems

/dev/sda1: UNEXPECTED INCONSISTENCY; RUN fsck MANUALLY.
         (i.e., without -a or -p options)
                                                  [FAILED]

*** An error occurred during the file system check.
*** Dropping you to a shell; the system will reboot
*** when you leave the shell.
Give root password for maintenance
(or type Control-D to continue):
```
錯誤訊息指出 `/dev/sda1` 分割區有不一致性的問題!!

依照指示，先輸入 root 密碼登入，再以 fsck 進行檔案系統的修復 (參數 -y 是為了讓 fsck 在檢測出錯誤時自動修復):

```
$ fsck -y /dev/sda1
fsck 1.39 (29-May-2006)
e2fsck 1.39 (29-May-2006)
/dev/sda1 contains a file system with errors, check forced.
Pass 1: Checking inodes, blocks, and sizes
...
Pass 2: Checking directory structure
...
Pass 3: Checking directory connectivity
...
Pass 4: Checking reference counts
...
Pass 5: Checking group summary information
...

/dev/sda1: ***** FILE SYSTEM WAS MODIFIED *****
/dev/sda1: ***** REBOOT LINUX *****
```

修復完畢，輸入 reboot 重新開機即可:

```
$ reboot
```