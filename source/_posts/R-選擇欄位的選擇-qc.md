title: R 選擇欄位的選擇 qc()
author: Hsien Ching Lo
tags:
  - R
categories: []
date: 2018-04-19 08:43:00
---
今天在 r-bloggers 看到這篇

[R Tip: Use Slices](https://www.r-bloggers.com/r-tip-use-slices/)

因此想到測試以下這段程式碼

```
library(wrapr)
library(dplyr)

# method1
system.time({
  mtcars %>% select(mpg, cyl, wt)
})

# method2
system.time({
  mtcars[, qc(mpg, cyl, wt)]
})
```

可以看到因為檔案小，但是這篇提供的 qc() 函數確實有比較快的速度，有時候 dplyr 會消耗到 0.05 秒，但是 qc() 一致都是 0 秒

```
> system.time({
+   mtcars %>% select(mpg, cyl, wt)
+ })
   user  system elapsed 
   0.02    0.00    0.01 
> system.time({
+   mtcars[, qc(mpg, cyl, wt)]
+ })
   user  system elapsed 
      0       0       0 
```