title: Windows 網路磁碟機設定與開機啟動
author: Hsien Ching Lo
date: 2018-03-19 08:22:52
tags:
---
進入開機路徑，AppData 預設是隱藏的要自行開啟隱藏資料夾

![](http://localhost:4000/images/pasted-0.png)


```
C:\Users\[使用者]\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
```

example 

```
C:\Users\sasva\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
```


example 


run_batch.bat

```
net use Y: \\192.168.171.143\sasva_data /user:sasva 1qaz@WSX
```