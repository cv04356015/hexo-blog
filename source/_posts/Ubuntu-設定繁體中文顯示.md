title: Ubuntu 設定繁體中文顯示
author: Hsien Ching Lo
tags:
  - Ubuntu
categories: []
date: 2018-03-31 10:58:00
---
# ubuntu server setting chinese

ref: [Ubuntu 用指令設定終端機顯示中文訊息](http://www.arthurtoday.com/2015/02/how-to-make-ubuntu-terminal-speak-your-language.html)

```
sudo locale-gen zh_TW 
sudo locale-gen zh_TW.UTF-8 
sudo dpkg-reconfigure locales 
sudo update-locale LANG="zh_TW.UTF-8" LANGUAGE="zh_TW"
```

ref: [解決錯誤訊息 perl: warning: Setting locale failed.](https://chingjs.wordpress.com/2017/03/02/linux-%E8%A7%A3%E6%B1%BA%E9%8C%AF%E8%AA%A4%E8%A8%8A%E6%81%AF-perl-warning-setting-locale-failed/)

```
vim .bashrc
```

keyin

```
export LC_ALL="zh_TW.UTF-8"
```

```
sudo locale-gen zh_TW.UTF-8
sudo dpkg-reconfigure locales
```


## .bashrc autorun

vim .bash_profile

add

```
source ~/.bashrc
```