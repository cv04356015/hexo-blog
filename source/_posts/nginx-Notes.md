title: nginx 筆記
author: Hsien Ching Lo
tags: []
categories: []
date: 2018-05-15 19:35:00
---
安裝 Nginx

```
$ sudo apt-get update
$ sudo apt-get install nginx
```

調整防火牆

```
# 列出防火牆資訊
$ sudo ufw app list
# 如果防火牆未開，需啟用防火牆
$ sudo ufw enable
# 允許 Nginx HTTP
$ sudo ufw allow 'Nginx HTTP'
# 查看防火牆狀態
$ sudo ufw status
```

服務相關指令

```
$ sudo systemctl status nginx
$ sudo systemctl stop nginx
$ sudo systemctl start nginx
$ sudo systemctl restart nginx
$ sudo systemctl reload nginx
$ sudo systemctl disable nginx
$ sudo systemctl enable nginx
```

重要文件位置

* 網頁內容 `/var/www/html`
* 主目錄 `/etc/nginx`
* 主要 nginx 配置文件(全局配置) `/etc/nginx/nginx.conf`
* 每個站點的服務氣模組的配置文件，透過 include 啟用 `/etc/nginx/sites-available`
* nginx 的服務請求 log `/var/log/nginx/access.log`
* nginx 的錯誤 log `/var/log/nginx/error.log`

網頁連結常常有對應連結錯誤的出現(ex. 圖片檔抓不到、JavaScript、CSS 讀取位置錯誤)，請新增以下三行。

```
proxy_set_header Host      $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
```

完整如下

```
server {
    listen 80;
    server_name localhost;

    charset     utf8;
    access_log    /var/log/nginx/example.access.log;

    # the default proxy_pass
    location /blog/ {
        proxy_pass http://127.0.0.1:4000/blog/;
        proxy_set_header Host      $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
```

# 加密

```
$ sudo sh -c "echo -n 'cv04356015:' >> /etc/nginx/.htpasswd"
$ sudo sh -c "openssl passwd -apr1 >> /etc/nginx/.htpasswd"
$ cat /etc/nginx/.htpasswd
$ sudo vim /etc/nginx/sites-enabled/default
```

在想要加密的地方新增

```
auth_basic "Restricted Content";
auth_basic_user_file /etc/nginx/.htpasswd;
```

ex.

```
server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    root /usr/share/nginx/html;
    index index.html index.htm;

    server_name localhost;

    location / {
        try_files $uri $uri/ =404;
        auth_basic "Restricted Content";
        auth_basic_user_file /etc/nginx/.htpasswd;
    }
}
```

重啟

```
$ sudo systemctl restart nginx
```

# 關閉某個網址

關閉網頁下某個網址 `https://r.cdc.gov.tw/shiny/application/`

```
location ^~ /shiny/application/ {
    deny all;
}
```

# 相關連結

* [nginx 安裝於 ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-in-ubuntu-16-04)
* [防火牆相關指令](http://www.arthurtoday.com/2013/12/ubuntu-ufw-add-firewall-rules.html)
* [How To Set Up Password Authentication with Nginx on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-password-authentication-with-nginx-on-ubuntu-14-04)