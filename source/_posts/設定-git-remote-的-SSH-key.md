title: 設定 git remote 的 SSH key
author: Hsien Ching Lo
tags:
  - Git
categories: []
date: 2018-03-31 11:29:00
---
# Git

## Set up an SSH key

***

```
ssh-keygen
```

```
Generating public/private rsa key pair.
Enter file in which to save the key (/home/rserver/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been save in /home/rserver/.ssh/id_rsa.
Your public key has been save in /home/rserver/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:11OlPLE4fVdbGsP2fi3GGC3p+xae30QmDrttoT/TZs4 rserver@rserver
The key's randomart image is:
+---[RSA 2048]----+
|             .+ +|
|             +oO+|
|            o+Ooo|
|           .+o.oo|
|        S ..+=..+|
|         .  o=*++|
|            .=.*o|
|            ooBo=|
|            .=+BE|
+----[SHA256]-----+
```

查詢密鑰匙

```
cat ~/.ssh/id_rsa.pub
```


## bitbucket 設定

***

bitbucket -> bitbucket 設置 -> SSH密鑰 -> 添加密鑰

確認 SSH 設置

```
ssh -T git@bitbucket.org
```

成功會看到 `You can use git or hg to connect to Bitbucket. Shell access is disable.`