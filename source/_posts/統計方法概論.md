title: 統計方法概論
author: Hsien Ching Lo
tags:
  - 統計方法
categories: []
date: 2018-03-31 09:59:00
---
* 判別分析(discriminant analysis)
 * [智庫百科](http://wiki.mbalib.com/zh-tw/%E5%88%A4%E5%88%AB%E5%88%86%E6%9E%90)
 * [Discriminant Analysis: Statistics All The Way](https://www.r-bloggers.com/discriminant-analysis-statistics-all-the-way/) 
 
 

判別分析 vs. 集群分析

* 判別分析（discriminant analysis），是在已知的分類之下，一旦遇到有新的樣本時，可以利用此法選定一判別標準，以判定如何該將新樣本放置於那個族群中。
* 集群分析（cluster analysis），則是希望將一群具有相關性的資料加以有意義的分類。