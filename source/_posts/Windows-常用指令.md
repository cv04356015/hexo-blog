title: Windows 常用指令
author: Hsien Ching Lo
tags: []
categories: []
date: 2018-03-31 11:54:00
---
# windows server ping port

伺服器管理員

```
telnet [-a][-e escape char][-f log file][-l user][-t term][host [port]]
 -a      嘗試自動登入。除了使用目前的登入使用者名稱之外，它和 -l 選項是相同的。
 -e      進入 telnet 用戶端提示的逸出字元。
 -f      用戶端記錄的檔案名稱
 -l      指定在遠端系統上所使用的登入使用者名稱。
         遠端系統必須能夠支援 TELNET ENVIRON 選項。
 -t      指定終端機類型。
         僅支援 vt100、vt52、ansi 及 vtnt 終端機類型。
 host    指定要連線的遠端電腦的主機名稱或 IP 位址。
 port    指定一個連接埠號碼或服務名稱。
```
