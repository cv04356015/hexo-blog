title: 刪除 Hyper-V 的保護資料的文件檔
author: Hsien Ching Lo
tags:
  - Hyper-V
categories: []
date: 2018-06-18 23:36:00
---
虛擬主機資料夾`D:\Hyper-V\Snapshots`下的 hyper-V 的文件檔，用於當機器部正常關機時，開機後可以恢復到原本關機前的狀態，用於資料保護。(因過度暫用實體空間，因此關閉客體作業系統)

* 設定 -> 管理 -> 自動停止動作 -> 關閉客體作業系統

# ref. 

1. [Hyper-V啟動虛擬機，消耗C盤大量磁盤空間](https://hk.saowen.com/a/f431a72be3f5be88a5d776cf6e651b3d9d4942ea9e36489f8c22627c37e04aab)