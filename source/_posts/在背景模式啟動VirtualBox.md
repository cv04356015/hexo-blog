title: 在背景模式啟動VirtualBox
author: Hsien Ching Lo
date: 2018-10-04 06:23:08
tags:
---

1. 建立一個 .bat 檔

```
cd /d "c:\Program Files\Oracle\VirtualBox\"
VBoxManage.exe startvm "ubuntu-16.04-shiny-server"  --type headless
```