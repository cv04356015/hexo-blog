title: Rancher 安裝建置
author: Hsien Ching Lo
date: 2018-04-03 09:17:35
tags:
---


```
sudo su
mkdir /opt/rancher
mkdir /opt/rancher/log
mkdir /opt/rancher/lib
mkdir /opt/rancher/log/mysql
mkdir /opt/rancher/lib/cattle
mkdir /opt/rancher/lib/mysql
sudo chmod -R 777 /opt/rancher/*
sudo docker run -d \
				-v /opt/rancher/log/mysql:/var/log/mysql \
				-v /opt/rancher/lib/cattle:/var/lib/cattle \
				-v /opt/rancher/lib/mysql:/var/lib/mysql \
                --restart=unless-stopped \
                -p 8088:8080 \
                --name rancher \
                rancher/server:stable
```

```
sudo docker run --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.9 http://192.168.171.247:8088/v1/scripts/812FDFEDC30190404863:1514678400000:rzpbpButCVTj4FQU5o62oZGTzYo
```

```
sudo docker ps -a
```