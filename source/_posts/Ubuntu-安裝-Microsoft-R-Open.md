title: Ubuntu 安裝 Microsoft R Open
author: Hsien Ching Lo
tags: []
categories: []
date: 2018-03-15 13:53:00
---
## 安裝於 Ubuntu Server 16.04 

On Ubuntu 14.04 - 16.04

* With root or sudo permissions, run the following commands:

Ref: https://docs.microsoft.com/en-us/machine-learning-server/r-client/install-on-linux

```
# Install as root or sudo
sudo su

# If your system does not have the https apt transport option, add it now
apt-get install apt-transport-https

# Set the package repository location containing the R Client distribution. 
# On Ubuntu 14.04.
# wget http://packages.microsoft.com/config/ubuntu/14.04/prod/packages-microsoft-prod.deb 
# On Ubuntu 16.04.
wget http://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb 

# Register the repo.
dpkg -i packages-microsoft-prod.deb

# Check for microsoft-prod.list configuration file to verify registration.
ls -la /etc/apt/sources.list.d/

# Update packages on your system
apt-get update

# Install the packages
apt-get install microsoft-r-client-packages-3.4.3

# List the packages
ls /opt/microsoft/rclient/
```

## 切換 Rstudio Server R 版本

```
$ vim /etc/rstudio/rsession.conf
```

新增

```
rsession-which-r=/opt/microsoft/rclient/3.4.3/runtime/R
```

重啟

```
sudo rstudio-server restart
```

或是由於 Rstudio Server 會預設抓取 `/usr/bin/R` 的位置

因此安裝兩次即可

```
sudo apt-get install microsoft-r-client-packages-3.4.3
sudo apt-get remove microsoft-r-*
sudo apt-get install microsoft-r-client-packages-3.4.3
```

* 移除原始 R (apt-get) 的更新清單

```
sudo vim /etc/apt/sources.list
```

* Rstudio Server 無法讀取套件

編輯 Renviron 檔

```
sudo vim /opt/microsoft/rclient/3.4.3/runtime/R/etc/Renviron
```

add

```
R_LIBS_SITE='/opt/microsoft/rclient/3.4.3/libraries/RServer'
```

* rJava 無法安裝

```
export JAVA_HOME='/usr/lib/jvm/java-1.8.0-openjdk-amd64/'
export PATH=$PATH:$JAVA_HOME/bin
sudo R CMD javareconf
```

* 無權限讀取套件

```
sudo chmod 777 /opt/microsoft/rclient/3.4.3/libraries/RServer
```

session 重開