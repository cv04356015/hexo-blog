title: SAS Viya CAS 資料庫連線
author: Hsien Ching Lo
date: 2018-03-21 17:14:01
tags:
---
# R 連線 CAS 資料庫

* [Github 連結](https://github.com/sassoftware/R-swat) 
* [swat 套件連結](https://developer.sas.com/apis/swat/r/v1.0.0/R-swat.pdf)

* 安裝 swat

``` R
library(devtools)
install_github("sassoftware/R-swat")
# install.packages('https://github.com/sassoftware/r-swat/archive/v1.2.0.tar.gz', repos=NULL, type='file')
```

連線程式碼如下：

查看是否連線到 8777 port，如果可以表示防火牆有通

``` bash
telnet cdc-sasviya.cdc.gov.tw 8777
```

如 R 於 Windows 端，需匯入憑證 vault-ca.crt (Viya 目錄 `/opt/sas/viya/config/etc/SASSecurityCertificateFramework/cacerts/`下可以找到)

ctrl+r -> certmgr.msc -> 授信任的根憑證授權單位 -> 憑證(右鍵) -> 所有工作 -> 匯入(vault-ca.crt)

``` R
library(swat)
conn <- swat::CAS('cdc-sasviya.cdc.gov.tw', 8777, protocol='https', username = "sasdemo01", password = "demopw")
```

# Python 連線 CAS 資料庫 

* [Github 連結](https://github.com/sassoftware/python-swat) 
* [python-swat 透過 https 連接](https://github.com/sassoftware/python-swat/issues/20)

先加入環境變數

``` bash
export CAS_CLIENT_SSL_CA_LIST=/opt/sas/viya/config/etc/SASSecurityCertificateFramework/cacerts/trustedcerts.pem
```

連線程式碼如下：

``` python
import swat
conn = swat.CAS("cdc-sasviya.cdc.gov.tw", 5570, username = "sasdemo01", password = "demopw")
```

在 Windows 版本下，需參考新增憑證的方式。[連結](http://go.documentation.sas.com/?cdcId=pgmsascdc&cdcVersion=9.4_3.3&docsetId=secref&docsetTarget=n12036intelplatform00install.htm&locale=zh-TW)