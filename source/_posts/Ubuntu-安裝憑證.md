title: Ubuntu 安裝憑證
author: Hsien Ching Lo
date: 2018-09-11 01:18:01
tags:
---

Given a CA certificate file foo.crt, follow these steps to install it on Ubuntu:

Create a directory for extra CA certificates in `/usr/share/ca-certificates`:

```
$ sudo mkdir /usr/share/ca-certificates/extra
```

Copy the CA `.crt` file to this directory:

```
sudo cp foo.crt /usr/share/ca-certificates/extra/foo.crt
```

Let Ubuntu add the `.crt` file's path relative to `/usr/share/ca-certificates` to `/etc/ca-certificates.conf`:
(套用憑證)

```
sudo dpkg-reconfigure ca-certificates
```

ref: https://askubuntu.com/questions/73287/how-do-i-install-a-root-certificate