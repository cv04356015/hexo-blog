title: R Shiny 的應用
author: Hsien Ching Lo
tags:
  - R
categories: []
date: 2018-03-31 13:51:00
---
# DataTable 不顯示其他功能(Search, footnote, ...)

use `dom = 't'`

```
library(DT)
d = cbind(
  head(iris),
  Mixed = c(1, 2, 3, '10', '1a', '1b')
)
datatable(d, options = list(dom = 't'))
```

# World cloud Click Event in shiny?

[function to give wordcloud2 click interactivity](https://github.com/Lchiffon/wordcloud2/issues/25)

```
wc2ClickedWord = function(cloudOutputId, inputId) {
  shiny::tags$script(shiny::HTML(
    sprintf("$(document).on('click', '#%s', function() {", cloudOutputId),
    'word = document.getElementById("wcSpan").innerHTML;',
    sprintf("Shiny.onInputChange('%s', word);", inputId),
    "});"
  ))
}
```

# Create a modal dialog UI

...


# 擷取操作設備框架大小於 shiny 之應用 

```
library(shiny)

# Define UI for application that draws a histogram
ui <- shinyUI(fluidPage(
  
  # Application title
  titlePanel("Old Faithful Geyser Data"),
  
  # Sidebar with a slider input for number of bins 
  sidebarLayout(
    sidebarPanel(
      tags$head(tags$script('
                            var dimension = [0, 0];
                            $(document).on("shiny:connected", function(e) {
                            dimension[0] = window.innerWidth;
                            dimension[1] = window.innerHeight;
                            Shiny.onInputChange("dimension", dimension);
                            });
                            $(window).resize(function(e) {
                            dimension[0] = window.innerWidth;
                            dimension[1] = window.innerHeight;
                            Shiny.onInputChange("dimension", dimension);
                            });
                            ')),
      sliderInput("bins",
                  "Number of bins:",
                  min = 1,
                  max = 50,
                  value = 30)
      ),
    
    # Show a plot of the generated distribution
    mainPanel(
      verbatimTextOutput("dimension_display"),
      plotOutput("distPlot")
    )
    )
))

# Define server logic required to draw a histogram
server <- shinyServer(function(input, output) {
  output$dimension_display <- renderText({
    paste(input$dimension[1], input$dimension[2], input$dimension[2]/input$dimension[1])
  })
  
  output$distPlot <- renderPlot({
    # generate bins based on input$bins from ui.R
    x    <- faithful[, 2] 
    bins <- seq(min(x), max(x), length.out = input$bins + 1)
    
    # draw the histogram with the specified number of bins
    hist(x, breaks = bins, col = 'darkgray', border = 'white')
  })
})

# Run the application 
shinyApp(ui = ui, server = server)
```

# shinyalert

這個 [Github](https://daattali.com/shiny/shinyalert-demo/) 是 shinyalert 的範例，可透過點選方式找到你要的效果。

或安裝完 shinyalert 後執行本段

```
runExample <- function() {
  appDir <- system.file("examples", "demo", package = "shinyalert")
  shiny::runApp(appDir, display.mode = "normal")
}
runExample()
```

簡單呈現

```
library(shiny)
library(shinyalert)

ui <- fluidPage(
  useShinyalert(),  # Set up shinyalert
  actionButton("preview", "Preview")
)

server <- function(input, output, session) {
  observeEvent(input$preview, {
    # Show a modal when the button is pressed
    shinyalert("Oops!", "Something went wrong.", type = "error")
  })
}

shinyApp(ui, server)
```