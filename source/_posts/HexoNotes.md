title: Hexo 筆記
date: 2018-03-18 19:48:26
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).


# 環境建置

--- 

* Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

* Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

* Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

* Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)

* 故障排除 - 建置錯誤

``` bash
$ hexo server
```
Ubuntu 的 Nodejs 版本過舊(os. 查了很久都是利用 nvm 重新安裝，但安裝不成，最後參考

[How To Install Node.js on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04)

之後發現 Ubuntu `apt-get` 給的是 nodejs 的 v4.2.6 版本你必須安裝到最新版

## 外掛套件

圖片讀取速度優化 - [Hexo-img-optimization](https://github.com/vkuznecovas/hexo-img-optimization) 



# Hexo 設定開機啟動

---


## Windows 設定

hexo-server-HsienloBlog.bat

```
cd /d D:\Users\User\Documents\My Projects\HsienloBlog
hexo s -d
```

hexo-server-HsienloBlog.vbs

```
set ws=WScript.CreateObject("WScript.Shell")
ws.Run "D:\\TaskScheduler\\hexo-server-HsienloBlog.bat /start",0
```

將 vbs 檔案放到 `Start Up` 下

```
C:\Users\User\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
```

## Ubuntu 設定

* 新增一個開機啟動
* 新增一個服務

寫一支 shell script，放在 `/etc/init.d` 底下

```
vim /etc/init.d/hexo.sh
```

hexo.sh

```
#!/bin/bash

cd /var/www/CDCDescriptiveDocuments/
nohup hexo server -d -p 4000 &

cd /var/www/CDCTechnicalDocuments/
nohup hexo server -d -p 4001 &
```

給予該腳本可執行權限

```
sudo chmod 755 hexo.sh
```

放在 /etc/init.d 底下，其實不會開機自啟動，只是用來收集腳本的而已

開機自啟動的偵測目錄，是在 `/etc/rcS.d`，而一般只放軟連結（符號連結），不放真實的檔案

```
cd /etc/rcS.d
sudo ln -s ../init.d/hexo.sh S50hexo.sh
```

/etc/rcS.d 中只有大寫 S 開頭的檔案，會被執行，檔名 S 後面的號碼，是執行優先順序，其實它是按照檔名排列順序，來依序執行；號碼小的檔名，自然會排在前面，就會被先執行；後碼大的檔名，會排在後面，就會比較晚被執行

解除 Link

```
sudo unlink S17hexo.sh
```

## 服務建立版本

建立 sh 檔案，讓服務執行

```
vim /usr/sbin/hexo.sh
```

```
#!/bin/bash
hexo server -d -p 4000
```

增加權限

```
sudo chmod +x /usr/sbin/hexo.sh
```

Create a service over systemctl

```
sudo vim /etc/systemd/system/hexo.service
```

```
[Unit]
Description=CDCTechnicalDocuments
After=network.target

[Service]
User=root
Group=root
ExecStart=/usr/sbin/hexo.sh
Restart=always
WorkingDirectory=/var/www/CDCTechnicalDocuments/

[Install]
WantedBy=multi-user.target
```

當程式錯誤可以自動重啟

```
Restart=always
```

檢查服務是否正確

```
sudo systemctl status hexo.service
```

正常如下

```
● hexotd.service - CDCTechnicalDocuments
   Loaded: loaded (/etc/systemd/system/hexotd.service; disabled; vendor preset: enabled)
   Active: inactive (dead)
```

運行服務

```
sudo systemctl start hexo.service
```

開機運行服務

```
sudo systemctl enable hexo.service
```

hexo 設定 nginx

```
server {
    listen 80;
    server_name localhost;

    charset     utf8;
    access_log    /var/log/nginx/example.access.log;

    # the default proxy_pass
    location /blog/ {
        proxy_pass http://127.0.0.1:4000/blog/;
        proxy_set_header Host      $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        # 針對網頁加密
        # auth_basic "Restricted Content";
        # auth_basic_user_file /etc/nginx/.htpasswd;
    }
}
```
# 圖片優化

图片无损压缩(Ubuntu)

```
# jpg file
sudo apt-get update
sudo apt-get install jpegoptim
sudo jpegoptim *.jpg
# png file
sudo apt-get update
sudo apt-get install optipng
sudo optipng *.png
```

# 其他套件

* 使用 hexo 繪製流程圖
	* [hexo-filter-flowchart](https://github.com/bubkoo/hexo-filter-flowchart)
    * [hexo-filter-sequence](https://github.com/bubkoo/hexo-filter-sequence)


# 相關連結

[如何将你的hexo博客部署在vps上？(自動部署)](https://crowncj.com/20170916.html)
[图片无损压缩](https://blog.csdn.net/hanshileiai/article/details/50835349)