title: SAS Viya 常用連結
author: Hsien Ching Lo
date: 2018-03-21 17:36:30
tags:
---




Viya 首頁

```
http://cdc-sasviya.cdc.gov.tw/SASHome/
```

Viya CAS 資料設定

```
https://192.168.171.248/cas-shared-default-http/tkcas.dsp
```

Viya SAS Studio

```
http://cdc-sasviya.cdc.gov.tw/SASStudio/
```

Viya 同步 143 的硬碟位置

```
cd /lib_core/
```

可用 `df` 查看硬碟連線狀況

mount 硬碟

```
mount.cifs \\\\192.168.171.143\\sasva_data /lib_core -o domain="CDCSASOA",user="sasva",pass="1qaz@WSX"
```

查看 Viya 服務狀況

```
/etc/init.d/sas-viya-all-services status
```