title: R 筆記
author: Hsien Ching Lo
date: 2018-06-06 12:49:43
tags:
---
# 資料處理

## 資料合併

* rbind.all 

當樣本合併時有時候欄位不同，R 會告知你合併上會有錯誤，如果使用 rbind.all 即可解決錯誤的狀況，合併的方法類似於 SAS 的 SET。


# 利用 R 寄信

* mailR
 * [rpremraj/mailR](https://github.com/rpremraj/mailR)
 * [A Utility to Send Emails from R](https://www.rdocumentation.org/packages/mailR/versions/0.4.1)
 
## 夾帶 html 文檔

```
send.mail(from = "sender@gmail.com",
          to = c("recipient1@gmail.com", "recipient2@gmail.com"),
          subject = "Subject of the email",
          body = "path.to.local.html.file",
          html = TRUE,
          inline = TRUE,
          smtp = list(host.name = "smtp.gmail.com", port = 465, user.name = "gmail_username", passwd = "password", ssl = TRUE),
          authenticate = TRUE,
          send = TRUE)
```

# Rmd 製作文檔

## 表格呈現、表格客製化

dataframe 直接呈現表格會沒有邊框，視覺效果不具有可讀性。

kableExtra 套件可以增加一些 kable 的特殊功能，例如 CSS、Hover 等功能，讓 html 的表格更好看些，湘戲用法可以參考：

[Create Awesome HTML Table with knitr::kable and kableExtra](https://cran.r-project.org/web/packages/kableExtra/vignettes/awesome_table_in_html.html)

上述功能程式碼如下：

```
kable(dt) %>%
  kable_styling(bootstrap_options = c("striped", "hover"))
```

## 增加目錄(索引)，增加可用性

[為你的 R Markdown 增加標題快捷列及表格美化](https://medium.com/@kstseng/r-markdown-template-a4b0449a56d5)

```
---
title: "R Markdown Template"
author: "hsienlo"
date: "2018/06/06"
output:
 html_document:
  toc: true
  toc_depth: 2
  toc_float:
   collapsed: false
   smooth_scroll: false
---
```

## 排程執行 Rmd 文件

透過 rmarkdown 的 render 函數可以執行 Rmd 文件

```
render(input, output_format = NULL, output_file = NULL, output_dir = NULL,
       output_options = NULL, intermediates_dir = NULL,
       knit_root_dir = NULL,
       runtime = c("auto", "static", "shiny", "shiny_prerendered"),
       clean = TRUE, params = NULL, knit_meta = NULL, envir = parent.frame(),
       run_pandoc = TRUE, quiet = FALSE, encoding = getOption("encoding"))
```

## 產生 html 文件

rmarkdown 產生 html 的[設定文件](https://rmarkdown.rstudio.com/html_document_format.html)

[markdown to html](https://github.com/jiankaiwang/sophia/blob/master/ci/ConvertMd2Html.py)


## 產生 pdf 文件

透過 rmarkdown 的 render 函數可以執行 Rmd 文件

pdf_document 函數可以控制 markdown 的選項(設置在 R markdown 文件頂端亦可)

rmarkdown 產生 PDF 的[設定文件](https://rmarkdown.rstudio.com/pdf_document_format.html)

```
# NOT RUN {
library(rmarkdown)

# simple invocation
render("input.Rmd", pdf_document())

# specify an option for latex engine
render("input.Rmd", pdf_document(latex_engine = "lualatex"))

# add a table of contents and pass an option to pandoc
render("input.Rmd", pdf_document(toc = TRUE, "--listings"))
# }
# NOT RUN {
# }
```

```
render(input, output_format = NULL, output_file = NULL, output_dir = NULL,
       output_options = NULL, intermediates_dir = NULL,
       knit_root_dir = NULL,
       runtime = c("auto", "static", "shiny", "shiny_prerendered"),
       clean = TRUE, params = NULL, knit_meta = NULL, envir = parent.frame(),
       run_pandoc = TRUE, quiet = FALSE, encoding = getOption("encoding"))
       
pdf_document(toc = FALSE, toc_depth = 2, number_sections = FALSE,
             fig_width = 6.5, fig_height = 4.5, fig_crop = TRUE,
             fig_caption = TRUE, dev = "pdf", df_print = "default",
             highlight = "default", template = "default", keep_tex = FALSE,
             latex_engine = "pdflatex", citation_package = c("none", "natbib",
             "biblatex"), includes = NULL, md_extensions = NULL, pandoc_args = NULL,
             extra_dependencies = NULL)
```

### 中文問題：pdf 無法產生含有中文的文件的錯誤

如出現錯誤

```
! Package inputenc Error: Unicode char \u8:年 not set up for use with LaTeX.
Try running pandoc with --latex-engine=xelatex.
```

先確認你的電腦(linux)要有

* TeX Live
* Latex

TeX Live 安裝[連結](http://tipsonubuntu.com/2016/09/16/install-tex-live-2016-ubuntu-16-04-14-04/)

```
$ sudo add-apt-repository ppa:jonathonf/texlive
$ sudo apt update && sudo apt install texlive-full
$ sudo apt update && sudo apt install texworks
```

LaTeX 安裝[連結](https://milq.github.io/install-latex-ubuntu-debian/)

可在文檔中新增參數

```
outputs:
  pdf_document:
    includes:
      in_header: header.tex
    latex_engine: xelatex
```

header.tex 範例如下

```
\usepackage{xeCJK}
\setCJKmainfont{文泉驛微米黑}  % 字体可以更换
\setmainfont{Georgia} % 設定英文字型
\setromanfont{Georgia} % 字型
\setmonofont{Courier New}
```

### 中文問題：pdf 成功產生，但中文變成空白

ctex 是 LaTeX 的 ctex 發行包提供的一個中文友好的宏包，同時提供文檔類，提交到CTAN，一般的發行版都有，很好用。但是在archlinux 下 texlive 使用時，xelatex 編譯提示找不到 SimKai 等中文字體的錯誤。

linux 環境下新增 windows 字體可參考：[解決ctex包在linux下找不到一些中文字體的問題](http://bbs.archlinuxcn.org/viewtopic.php?id=759)

* [Latex in Ubuntu](http://pangomi.blogspot.com/2012/11/latex-in-ubuntu.html)
* [Latex in Windows](http://leavedcorn.pixnet.net/blog/post/24773932)


* 查看現有的中文字體

```
$ fc-list :lang=zh-cn
```

* 查看所有字體

```
$ fc-list - list available fonts
```


這兩個是在安裝完 zh_tw.utf-8 給出來的

```
/usr/share/fonts/truetype/wqy/wqy-microhei.ttc: 文泉驛微米黑,WenQuanYi Micro Hei,文泉驿微米黑:style=Regular
/usr/share/fonts/truetype/wqy/wqy-microhei.ttc: 文泉驛等寬微米黑,WenQuanYi Micro Hei Mono,文泉驿等宽微米黑:style=Regular
```

# 資料視覺化

## 在 Ubuntu 下使用標楷體

```
$ sudo vim /etc/fonts/conf.d/30-cjk-aliases.conf
```

註解起來(標楷體被AR PL UKai TW跟AR PL ZenKai Uni代用)

```
<!--
<alias>
<family>標楷體</family>
<accept><family>AR PL UKai TW</family></accept>
<accept><family>AR PL ZenKai Uni</family></accept>
</alias>
-->
```

```
$ mkdir ~/.fonts
```

把 windows 裡的 `c:\windows\fonts\kaiu.ttf` 複製到 `~/.fonts/` 下面

```
$ sudo fc-cache -f -v
```

從新登入使用者後

```
$ fc-list | grep Kai
```

標楷體,DFKai\-SB:style=Regular

## 設置點與線

ref: https://www.douban.com/group/topic/96637962/

![](/blog/images/pasted-29.png)

```
#根據變量cond調整點形狀和線類型 
ggplot(df, aes(x=xval, y=yval, group = cond)) + 
geom_line(aes (linetype=cond), #線的類型取決於cond 
size = 1.5) + #粗線 
geom_point(aes(shape=cond), #點形狀取決於cond 
size = 4) #大型點標記 
```

![](/blog/images/pasted-30.png)


## 一個圖檔合併的實例

[Plotting tables alsongside charts in R](https://magesblog.com/post/2015-04-14-plotting-tables-alsongside-charts-in-r/)

![](https://magesblog.com/img/magesblog/TwoPlotsInOne.png)

```
# Create some sample data
CV_1 <- 0.2
CV_2 <- 0.3
Mean <- 65
sigma_1 <- sqrt(log(1 + CV_1^2))
mu_1 <- log(Mean) - sigma_1^2 / 2
sigma_2 <- sqrt(log(1 + CV_2^2))
mu_2 <- log(Mean) - sigma_2^2 / 2
q <- c(0.25, 0.5, 0.75, 0.9, 0.95) 
SummaryTable <- data.frame(
  Quantile=paste0(100*q,"%ile"), 
  Loss_1=round(qlnorm(q, mu_1, sigma_1),1),
  Loss_2=round(qlnorm(q, mu_2, sigma_2),1)
  )
# Create a plot 
library(ggplot2)
plt <- ggplot(data.frame(x=c(20, 150)), aes(x)) + 
  stat_function(fun=function(x) dlnorm(x, mu_1, sigma_1), 
                aes(colour="CV_1")) + 
  stat_function(fun=function(x) dlnorm(x, mu_2, sigma_2), 
                aes(colour="CV_2")) +
  scale_colour_discrete(name = "CV", 
                        labels=c(expression(CV[1]), expression(CV[2]))) +
  xlab("Loss") +  
  ylab("Density") +
  ggtitle(paste0("Two log-normal distributions with same mean of ",
                 Mean,", but different CVs")) 
# Create a table plot
library(gridExtra)
names(SummaryTable) <- c("Quantile", 
              expression(Loss(CV[1])),
              expression(Loss(CV[2])))
# Set theme to allow for plotmath expressions
tt <- ttheme_default(colhead=list(fg_params = list(parse=TRUE)))
tbl <- tableGrob(SummaryTable, rows=NULL, theme=tt)
# Plot chart and table into one object
grid.arrange(plt, tbl,
             nrow=2,
             as.table=TRUE,
             heights=c(3,1))
```

# 建立 xts 序列(畫圖用)

* 當 highchart 無法對應日期時使用此序列

```
xts::xts(x = NULL,
    order.by = index(x),
    frequency = NULL,
    unique = TRUE,
    tzone = Sys.getenv("TZ"),
    ...)
```

example 

```
xts([y vector], order.by=[date vector])
```

## ggplot2


## plotly

* [股價圖](https://plot.ly/r/candlestick-charts/)


## highchart


# 如何比較測試程式碼執行速度

rbenchmark 套件可以同時比較

```
library(data.table)
library(rbenchmark)

DT = data.table(x=rep(c("a","b",NA),each=3e6), y=c(1,3,6), v=1:9)
setkey(DT,x)  

benchmark(DT["a",],
          DT[is.na(x),],
          replications=20)
```

相關連結: 

1. [5 ways to measure running time of R code](http://www.alexejgossmann.com/benchmarking_r/)

# LINE Notify

```
library(httr)
msg <- list(
  message = paste0("Hello World")
)
httr::POST(
  "https://notify-api.line.me/api/notify/",
  add_headers(Authorization = "Bearer L4IEQOF561w3DmfVocjPP7zTb5GCbor0PjEJMkGT6HB"),
  content_type("multipart/form-data"),
  body = msg
)
````