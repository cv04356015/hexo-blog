title: R 讀寫資料庫
author: Hsien Ching Lo
tags:
  - PostgreSQL
categories:
  - 資料庫
date: 2018-03-13 13:10:00
---
# PostgreSQL

利用 R 讀取 PostgreSQL
- RPostgreSQL::dbConnect 連接 PostgreSQL
- 設定 ODBC，DBI::dbConnect 連接 odbc::odbc()
- 設定 ODBC，RODBC::odbcConnect 連接 odbc::odbc()
- 讀取後自行轉碼

## 將資料寫入 PostgreSQL 資料庫

- dbConnect 連接 PostgreSQL

```
library(RPostgreSQL)
drv <- dbDriver("PostgreSQL")
con <- dbConnect(drv, dbname = "[database]", host = "localhost", port = 5432, user = "postgres", password = [password])
dbWriteTable(con, "[table name]", value = df, overwrite = TRUE, row.names = FALSE)
```

- 設定 ODBC，RODBC::odbcConnect 連接 odbc::odbc()

寫入資料庫不需要 DBMSencoding 參數，否則在寫入時會出錯，且利用 RODBC 方式寫入 PostgreSQL 後，讀取可以省略 `DBMSencoding = "BIG5"` 參數設定，但如果使用 RPostgreSQL 或 DBI 寫入 PostgreSQL，則需要 `DBMSencoding = "BIG5"` 參數設定。

```
library(RODBC)
con <- odbcConnect(dsn = "[dsn: odbc name]")
# sqlDrop(con, "[table name]")
sqlSave(con, dat = dt, tablename = "[table name]", rownames = FALSE)
odbcClose(con)
# odbcCloseAll()
```

sqlSave 參數設定[參考](https://www.rdocumentation.org/packages/RODBC/versions/1.3-15/topics/sqlSave)

- safer 是否要覆蓋資料表:logical. If true, create a non-existing table but only allow appends to an existing table. If false, allow sqlSave to attempt to delete all the rows of an existing table, or to drop it.
- append 新增資料於原資料表後方

需注意的是，如果不是第一次建立表格需先用 sqlDrop 刪除表格，在進行新增，如果使用 safer 很容易造成錯誤。

## 讀取 PostgreSQL 資料庫

- dbConnect 連接 PostgreSQL

```
drv <- dbDriver("PostgreSQL")
con <- dbConnect(drv, dbname = "[database]", host = "[ip]", port = 5432, user = "[username]", password = [password])
df_postgres <- dbGetQuery(con, "SELECT * from [table]")
```

- 設定 ODBC，DBI::dbConnect 連接 odbc::odbc()

```
library(DBI)
con <- dbConnect(odbc::odbc(), "[odbc name]")
df_postgres <- dbGetQuery(con, 'SELECT * from "[table]"')
```

- 設定 ODBC，RODBC::odbcConnect 連接 odbc::odbc()

RODBC 對於編碼的處理較好，windows 會有 utf-8 編碼的問題，需透過 DBMSencoding 參數解決。

```
library(RODBC)
channel <- odbcConnect(dsn = "[odbc name]", DBMSencoding = "BIG5")
df_postgres <- sqlQuery(channel, 'SELECT * from "[table]"')
```

一般資料讀取

```
library(RODBC)
channel <- odbcConnect(dsn = "[odbc name]")
df_postgres <- sqlQuery(channel, 'SELECT * from "[table]"')
```

- 讀取後自行轉碼

轉碼函數如下

```
declare_utf8 <- function(x) {
  Encoding(x) <- "UTF-8"
  x
}
declare_big5 <- function(x) {
  Encoding(x) <- "BIG5"
  x
}
```


```
library(DBI)
con <- dbConnect(odbc::odbc(), "[odbc name]")
df_postgres <- dbGetQuery(con, 'SELECT * from "[table]"')
result_df <- df %>% mutate_if(is.character, declare_big5)
```

## 指令執行完成後關閉

指令執行完成後關閉，否則會處於鎖定狀態

```
dbDisconnect(con)
dbUnloadDriver(drv)
```

# Oracle

## Oracle Database 目前有哪些 NLS 設定, 其設定值為何, 可用下面語法 :

```
dbGetQuery(con, "select * from nls_database_parameters;")
dbGetQuery(con, "select * from nls_database_parameters where parameter='NLS_CHARACTERSET';")
dbGetQuery(con, "SELECT * FROM V$NLS_VALID_VALUES WHERE parameter = 'CHARACTERSET'")
```

會查到 `AMERICAN_AMERICA.ZHT16MSWIN950`

因此需要設定環境變數才可以正常讀取

```
Sys.setenv(LANG="AMERICAN_AMERICA.ZHT16MSWIN950")
```

> RODBC 等相關套件在讀取 SQL 資料庫時會造成相當大的負擔，ㄧ般需要17.5倍的記憶體容量或是40倍的硬碟空間，因此如果檔案很大，但硬體無法負擔時，盡量不要使用。

