title: https 設定
author: Hsien Ching Lo
date: 2018-05-28 02:05:33
tags:
---
# 設定 SSL on nginx 以 cdc 為例

SSL 文件的取得

```
$ sudo apt-get install openssl
$ sudo mkdir mkdir /etc/nginx/ssl
$ sudo cp cdc.gov.tw.crt /etc/nginx/ssl
$ sudo cp cdc.gov.tw.pfx /etc/nginx/ssl
```

## setup SSL using .pfx files

將 pfx 轉為 rsa (需輸入密碼)

```
openssl pkcs12 -in ./YOUR-PFX-FILE.pfx -nocerts -nodes -out domain.rsa
```

## setup nginx config

```
$ sudo vim /etc/nginx/sites-available/default
```

新增 ssl 的內容

```
server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name r.cdc.gov.tw;

    ssl on;
    ssl_certificate /etc/nginx/ssl/cdc.gov.tw.crt;
    ssl_certificate_key /etc/nginx/ssl/cdc.gov.tw.rsa;
    
...

}
```

### 新增一個自動導向為 https 的段落

```
server {

    listen 80;
    listen [::]:80;
    server_name r.cdc.gov.tw;
    return 301 https://r.cdc.gov.tw$request_uri;

}
```

ref: https://blog.knoldus.com/easiest-way-to-setup-ssl-on-nginx-using-pfx-files/


# 設定 SSL on nginx 以個人網頁為例(申請免費dns)


## 申請 no-ip 

* https://my.noip.com/#!/dynamic-dns




## 申請 SSL


```
sudo apt-get update
sudo apt-get install -y git
sudo git clone https://github.com/letsencrypt/letsencrypt /home/https/letsencrypt
cd /home/https/letsencrypt
sudo ./letsencrypt-auto

# 權限
cd /var/www
sudo mkdir letsencrypt
sudo chown www-data:www-data letsencrypt

# 編輯 nginx
sudo vim /etc/nginx/sites-available/default
```

```
server{
…
       location /.well-known/acme-challenge {
                root /var/www/letsencrypt;
       }
…
}
```

```
cd /home/https
mkdir configs
sudo vim /home/https/configs/default.conf
```

```
domains = hsienlo.ddns.net
rsa-key-size = 4096
server = https://acme-v01.api.letsencrypt.org/directory
email = cv04356015@gmail.com
text = True
authenticator = webroot
webroot-path = /var/www/letsencrypt/
```

```
sudo service nginx reload
cd /home/https/letsencrypt
# 註冊
sudo ./letsencrypt-auto --config /home/test/configs/default.conf certonly
# 編輯 nginx
sudo vim /etc/nginx/sites-available/default
```

```
server {
listen 443 ssl default_server;
server_name localhost;

ssl_certificate /etc/letsencrypt/live/hsienlo.ddns.com/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/hsienlo.ddns.com/privkey.pem;

...
}
```

```
sudo service nginx reload
```


## 設定排程

```
cd /home/https
sudo vim renew-letsencrypt.sh
```

```
#!/bin/sh

cd /home/https/letsencrypt/
./letsencrypt-auto --config /home/https/configs/default.conf certonly

if [ $? -ne 0 ]
 then
     ERRORLOG=`tail /var/log/letsencrypt/letsencrypt.log`
     echo -e "The Let's Encrypt cert has not been renewed! n n"
           $ERRORLOG
 else
     nginx -s reload
fi

exit 0
```

```
crontab -e
```

```
0 0 1 JAN,MAR,MAY,JUL,SEP,NOV * /home/https/renew-letsencrypt.sh
```

相關連結

* [讓網頁連線加密(https)！如何讓網頁伺服器取得免費SSL憑證】(https://www.astralweb.com.tw/get-free-web-server-ssl-certificate-letsencrypt/)
