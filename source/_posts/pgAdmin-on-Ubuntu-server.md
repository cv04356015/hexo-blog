title: pgAdmin on Ubuntu server
author: Hsien Ching Lo
date: 2018-05-28 05:04:55
tags:
---

# Docker run

```
sudo docker run -p 8888:80 -d \
-v "/private/var/lib/pgadmin:/var/lib/pgadmin" \
-e "PGADMIN_DEFAULT_EMAIL=eic@cdc.gov.tw" \
-e "PGADMIN_DEFAULT_PASSWORD=1qaz@WSX" \
--name pgadmin4 \
dpage/pgadmin4
```

# service

```
sudo vim /usr/sbin/phadmin.sh
```

pgadmin 會回傳 exit 0 導致無線重啟，需設定如下

```
#!/bin/bash
{
if [ "$(docker inspect -f {{.State.Running}} pgadmin4)" == "true" ] 
then
	echo "OK, continue"
elif [ "$(sudo docker ps -f "name=pgadmin4" -aq)" == "" ] 
then 
	sudo docker run -p 8888:80 -d \
	-v "/private/var/lib/pgadmin:/var/lib/pgadmin" \
	-e "PGADMIN_DEFAULT_EMAIL=eic@cdc.gov.tw" \
	-e "PGADMIN_DEFAULT_PASSWORD=1qaz@WSX" \
	--name pgadmin4 \
	dpage/pgadmin4
else 
	sudo docker restart pgadmin4
fi
} || {
	sudo docker run -p 8888:80 -d \
	-v "/private/var/lib/pgadmin:/var/lib/pgadmin" \
	-e "PGADMIN_DEFAULT_EMAIL=eic@cdc.gov.tw" \
	-e "PGADMIN_DEFAULT_PASSWORD=1qaz@WSX" \
	--name pgadmin4 \
	dpage/pgadmin4
}
```

```
sudo chmod +x /usr/sbin/pgadmin.sh
sudo vim /etc/systemd/system/pgadmin.service
```

```
[Unit]
Description=pgAdmin
After=network.target

[Service]
User=root
Group=root
ExecStart=/usr/sbin/pgadmin.sh
Restart=always
WorkingDirectory=/

[Install]
WantedBy=multi-user.target
```


```
sudo systemctl status pgadmin.service
sudo systemctl start pgadmin.service
sudo systemctl enable pgadmin.service
```
