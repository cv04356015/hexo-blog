title: Linux R 繪圖中文問題
author: Hsien Ching Lo
tags:
  - R
categories: []
date: 2018-03-22 17:33:00
---

在 Linux 中 R 無法正常顯示中文

需安裝字體

```
sudo apt-get install ttf-wqy-microhei
```

在 R 執行測試指令即可

```
plot(1, main = "中文")
```

如果需要變更其他字體，則需要安裝套件 Cairo

前置於 ubuntu 需要安裝的開發軟件有 libcairo2-dev 與 libxt-dev

```
sudo apt-get install libcairo2-dev
sudo apt-get install libxt-dev
```

不指定 cran 預設為 mran

```
sudo su -c "R -e \"install.packages('Cairo')\""
```

在 R 執行測試指令 regular, bold 放置其他參數

```
library(Cairo)
CairoFonts(regular = "WenQuanYi Micro Hei", bold = "WenQuanYi Micro Hei")
plot(1, main = "中文")
```