title: Oracle Notes
author: Hsien Ching Lo
date: 2018-05-30 01:45:17
tags:
---
[時間函數](#時間函數)

## ROWNUM 關鍵字

ref:https://blog.csdn.net/null____/article/details/8264654

* 列出前 n 行

```
SELECT column_name(s) FROM table_name WHERE ROWNUM <= number 
```

* 列出最大值

-- 透過排序達到更大的活用

```
SELECT  procedure_no  FROM (SELECT  *  FROM process_card_procedure where process_card_id=421 order by cast(procedure_no as int) desc) where rownum<=1   
```

# 當 in 的參數超過 1000 個時需另找方法解決

* Union

```
select * from TestTable where ID in (1,2,3,4,...,1000)
union all
select * from TestTable where ID in (1001,1002,...)
```

* OR 

```
select * from TestTable where ID in (1,2,3,4,...,1000) or ID in (1001,1002,...,2000)
```

# 計算類型

## 函數
* count()

# 時間類型

## 函數
* trunc() 將日期時間格式轉為日期格式
```
```

## 篩選

Q1: 近 60 天的日期篩選

A1:
```
SELECT *
FROM [TABLE]
WHERE [DATE_VAR] > (CURRENT_TIMESTAMP - 60);
```

Q2: 篩選兩個時間段之間的資料

A2:
```
SELECT *
FROM [TABLE]
WHERE [DATE_VAR] between date '2017-12-01' and date '2017-12-31';
```

Q3: 篩選大於某個時間點以後的資料

A3:
```
select trunc(admit_date), sum(rods_rs) as rods_rs, sum(total) as total
from CDCDW.V_FACT_RODS_REPORT
where trunc(admit_date) >= to_date('20180701', 'yyyymmdd')
group by trunc(admit_date);
```