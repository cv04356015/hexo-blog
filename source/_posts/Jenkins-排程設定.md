title: Jenkins 排程設定
author: Hsien Ching Lo
date: 2018-05-08 10:45:04
tags:
---

# 排程新增

建置觸發成續勾選定期建置

![](/blog/images/pasted-21.png)

寫法可參照說明

```
This field follows the syntax of cron (with minor differences). Specifically, each line consists of 5 fields separated by TAB or whitespace:
MINUTE HOUR DOM MONTH DOW
MINUTE	Minutes within the hour (0–59)
HOUR	The hour of the day (0–23)
DOM	The day of the month (1–31)
MONTH	The month (1–12)
DOW	The day of the week (0–7) where 0 and 7 are Sunday.
To specify multiple values for one field, the following operators are available. In the order of precedence,

* specifies all valid values
M-N specifies a range of values
M-N/X or */X steps by intervals of X through the specified range or whole valid range
A,B,...,Z enumerates multiple values
```

這裡提供特別的寫法

* 每個月的第一個禮拜天

```
0 7 1-7 * 0
```

建置環境的部分可以勾選，在畫面輸出中加入時間戳記

![](/blog/images/pasted-22.png)

建置執行 shell

如果需要每當執行錯誤時，排除後面的處理程序，可以這樣寫

```
Rscript /home/rserver/task/MT_DATA/MT_DATA.R
if [ $? = 1 ]; then 
    echo "Your MT_DATA script exited with exit status 1" 
	exit 1 
else 
    echo "Your MT_DATA script exited with exit status 0" 
	Rscript /home/rserver/task/MT_DOH/MT_DOH.R
	if [ $? = 1 ]; then 
		echo "Your MT_DOH script exited with exit status 1" 
		exit 1 
	else 
		echo "Your MT_DOH script exited with exit status 0" 
		Rscript /home/rserver/task/MT_CLUSTER/MT_CLUSTER.R
		if [ $? = 1 ]; then 
			echo "Your MT_CLUSTER script exited with exit status 1" 
			exit 1 
		else 
			echo "Your MT_CLUSTER script exited with exit status 0" 
			Rscript /home/rserver/task/MT_MAIL/MT_XLSX.R
			if [ $? = 1 ]; then 
				echo "Your MT_XLSX script exited with exit status 1" 
				exit 1 
			else 
				echo "Your MT_XLSX script exited with exit status 0" 
				python  /home/rserver/task/MT_MAIL/MT_MAIL.py
				if [ $? = 1 ]; then 
					echo "Your MT_MAIL script exited with exit status 1" 
					exit 1 
				else 
					echo "Your ALL script exited with exit status 0" 
					exit 0
				fi
			fi
		fi
	fi
fi
```

建置後動作，可新增電子郵件通知

![](/blog/images/pasted-23.png)

使用前要先至「管理 Jenkins -> 系統設定」進行 email 的設置

![](/blog/images/pasted-24.png)

![](/blog/images/pasted-25.png)


# 環境變數設定

Jenkins 使用者的環境路徑可能會和管理員不同，因此如發現排程無法正常執行有可能是環境變數的問題

可至管理 Jenkins -> 系統設定 -> 全域屬性做設定

注意： Path 不能寫成 PATH

![](/blog/images/pasted-20.png)

