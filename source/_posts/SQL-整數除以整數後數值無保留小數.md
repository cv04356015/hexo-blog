title: SQL 整數除以整數後數值無保留小數
author: Hsien Ching Lo
tags:
  - SQL
categories: []
date: 2018-06-29 08:37:00
---

整數除以整數任何小數點後數字部份都會截斷。

但只要其中一個是浮點數，計算結果就是浮點數了。

```sql
select cast(51 as float)/100
select cast(51 as float)/cast(100 as float)
```