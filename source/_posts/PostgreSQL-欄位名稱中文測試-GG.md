title: PostgreSQL 欄位名稱中文測試(GG)
author: Hsien Ching Lo
tags:
  - PostgreSQL
categories:
  - 資料庫
date: 2018-03-13 10:13:00
---
以 mtcars 做為範例

```
library(RPostgreSQL)
library(magrittr)

df <- mtcars
df <- df %>% set_colnames(c("油耗", "汽缸數", "單汽缸排氣量", "馬力", "後輪比", "車重", "qsec", "vs", "自手排", "前進檔", "化油器"))
pw <- "[password]"
drv <- dbDriver("PostgreSQL")
con <- dbConnect(drv, dbname = "[database]", host = "localhost", port = 5432, user = "postgres", password = pw)
rm(pw)
dbWriteTable(con, "mtcars", value = df, overwrite = TRUE, row.names = FALSE)
```

無法寫入(已測試 CP950 與 UTF-8 撰寫程式碼)

```
Error in postgresqlExecStatement(conn, statement, ...) : 
  RS-DBI driver: (could not Retrieve the result : ERROR:  invalid byte sequence for encoding "UTF8": 0xaa
)
[1] FALSE
Warning message:
In postgresqlWriteTable(conn, name, value, ...) :
  could not create table: aborting assignTable
```

僅限英文欄位

```
library(RPostgreSQL)
library(magrittr)

df <- mtcars
pw <- "[password]"
drv <- dbDriver("PostgreSQL")
con <- dbConnect(drv, dbname = "[database]", host = "localhost", port = 5432, user = "postgres", password = pw)
rm(pw)
dbWriteTable(con, "mtcars", value = df, overwrite = TRUE, row.names = FALSE)
```

```
[1] TRUE
```