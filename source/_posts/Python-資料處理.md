title: Python 資料處理
author: Hsien Ching Lo
date: 2018-05-28 02:30:53
tags:
---

# 建立 dataFrame 與資料合併

```
import pandas as pd

lkey = ["foo", "bar", "baz", "foo"]
value = [1,2,3,4]
A = {"lkey": lkey, "value": value}
A = pd.DataFrame(A)
rkey = ["foo", "bar", "qux", "bar"]
value = [5,6,7,8]
B = {"rkey": rkey, "value": value}
B = pd.DataFrame(B)

A.merge(B, left_on='lkey', right_on='rkey', how='outer')
```