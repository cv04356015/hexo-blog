title: R 與資料庫的運用
author: Hsien Ching Lo
date: 2018-09-14 07:28:11
tags:
---
[表列資料庫中的所有資料表資訊以 SQL Server 為例](#表列資料庫中的所有資料表資訊以 SQL Server 為例)


## 表列資料庫中的所有資料表資訊以 SQL Server 微例

```
library(RODBC)
con_NHI_ODBC <- odbcConnect(dsn = "NHI_ODBC", uid = "user", pwd = "password")
NHI_TABLE_LIST <- sqlQuery(con_NHI_ODBC, 'SELECT * FROM INFORMATION_SCHEMA.TABLES', as.is = T)
odbcClose(con_NHI_ODBC)
```