title: Line App on Python
author: Hsien Ching Lo
date: 2018-08-21 06:56:26
tags:
---

# LINE通知功能

```
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 17:25:57 2018

@author: Hsien-Ching Lo
"""


import sys
# -----------------------------------------------------------------------------
# you can comment it, there is only one function using it (sendMsgByLineTool)
# import lineTool
# -----------------------------------------------------------------------------
import requests

# def sendMsgByLineTool(getToken, getMsg):
#    lineTool.lineNotify(getToken, getMsg)
    
def sendMsgByOriginPackage(getToken, getMsg):
    url = 'https://notify-api.line.me/api/notify'
    headers = {
        'Authorization': 'Bearer ' + getToken, 
        'Content-Type' : 'application/x-www-form-urlencoded'
    }
    payload = {'message': getMsg}
    r = requests.post(url, headers = headers, params = payload)
    return r
    
def sendMsgAndStickerByOriPkg(getToken, getMsg, getStickerPkgId, getStickerId):
    url = "https://notify-api.line.me/api/notify"
    headers = {
        "Authorization": "Bearer " + getToken
    }
    payload = {\
        "message": getMsg, \
        "stickerPackageId": getStickerPkgId, \
        'stickerId': getStickerId
    }
    r = requests.post(url, headers = headers, params = payload)
    return r
    
def sendMsg_imageFullsize(getToken, getMsg, getPicUrl):
    url = "https://notify-api.line.me/api/notify"
    headers = {
        "Content-Type" : "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + getToken
    }
    payload = {'message': getMsg,
               'imageThumbnail':getPicUrl,
               'imageFullsize': getPicUrl}
    r = requests.post(url, headers = headers, data = payload)
    return r

# necessary
token = 'urfDi7C1AArmqZPVg4IhPsL6klfYYDcHEc3fqdynOQz'

# only send the message
msg = sys.argv[1]
print(sendMsgByOriginPackage(token, msg).status_code)

# send the message and the sticker
# msg = "send the message and the sticker"
# stickerPId = 1
# stickerId = 114
# print(sendMsgAndStickerByOriPkg(token, msg, stickerPId, stickerId).status_code)

 # send the message and the image
msg = "send the message and the image"
imgPath = "https://cdcdataapi.azurewebsites.net/data_stat_img.jpeg"
sendMsg_imageFullsize(token, msg, imgPath).text

sendMsg_imageFullsize(token, msg, imgPath)
```


# LINE推播功能

```
import os
from linebot import LineBotApi
from linebot.models import TextSendMessage
from linebot.exceptions import LineBotApiError

line_bot_api = LineBotApi('L+bDuYSwo2Y30Dy+Xv3nbnRvoFouZfi3ss18yBTh/QXjWW0GwvjECFrl26kTN+SRZW/nsik7ZEEJX+CFq2SollO6KiqTs3K24GMi5IwBCWvloExaZMfI5olG0IYG6RqPrypvqyzXajWr9j5GQcfFNgdB04t89/1O/w1cDnyilFU=')

line_bot_api.push_message('6k43fTIHVS', TextSendMessage(text='test'))
```