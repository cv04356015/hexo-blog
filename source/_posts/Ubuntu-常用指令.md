title: Ubuntu 常用指令
author: Hsien Ching Lo
tags:
  - Ubuntu
categories: []
date: 2018-03-31 11:25:00
---
## 使用者相關

新增使用者 ( Add User ) useradd

```
sudo useradd -m [username] -p [password(crypt3)]
```

刪除使用者 ( Delete User ) deluser

```
sudo deluser [username] --remove-all-files
```

搜尋使用者 ( Search for users )

```
cut -d: -f1 /etc/passwd
```

使用者清單

```
cut -d: -f1 /etc/passwd
```

盡量以 adduser 與 deluser 操作使用者，例如 `--remove-all-files` 可以清除殘留資訊與群組等。

但可能缺少某些參數，我們會以 useradd 新增使用者

對使用者新增群組

```
useradd -g {group-name} username
useradd -G {group-name} username
```

大寫表示一次新增多個，以逗號分隔

```
useradd -G admins,www,rd daniel
```

修改已存在帳號的群組

```
usermod -g admins andrew
```



## 系統相關 

刪除執行緒

```
# -9 最高權限
sudo kill -9 [PID] 
```

搜尋功能 (| grep)

```
top | grep '11648'
```

## 查詢 CPU、RAM

CPU Core數量(包含Hyper Thread, HT)

```
# cat /proc/cpuinfo|grep "model name"|wc -l
```

實體CPU型號與數量

```
# dmidecode -t processor|grep "Version:"
```

Memory總容量(此方法不適用有裝Xen且有VM佔用Memory的情況)

```
# cat /proc/meminfo |grep "Total"
```

有幾條實體Memory(此方法可以看出實際Memory數量)

```
# dmidecode -t memory|grep "Size:"
```

# 解壓縮指令

```
.tar
打包：tar cvf FileName.tar DirName
解包： tar xvf FileName.tar

.gz
壓縮：gzip FileName
解壓1：gunzip FileName.gz
解壓2：gzip -d FileName.gz

.tar.gz
壓縮：tar zcvf FileName.tar.gz DirName
解壓：tar zxvf FileName.tar.gz

.bz2
壓縮： bzip2 -z FileName
解壓1：bzip2 -d FileName.bz2
解壓2：bunzip2 FileName.bz2

.tar.bz2
壓縮：tar jcvf FileName.tar.bz2 DirName
解壓：tar jxvf FileName.tar.bz2

.tgz
壓縮：unkown
解壓：tar zxvf FileName.tgz

.tar.tgz
壓縮：tar zcvf FileName.tar.tgz FileName
解壓：tar zxvf FileName.tar.tgz

.zip
壓縮：zip FileName.zip DirName
解壓：unzip FileName.zip

.rar
壓縮：rar a FileName.rar
解壓：rar e FileName.rar

.lha
壓縮：lha -a FileName.lha FileName
解壓：lha -e FileName.lha
```

# umask 指令

Linux 將檔案分成三種身份、四種權限：

三種身份是：

* u: 自己(user)
* g: 和自己同一組的人(group)
* o: 其它人 (other)

而 a 則是代表所有的人，每種身份皆有四種可能的權限:

* r : 讀取權 (read)
* w : 寫入權 (write)
* x : 執行權 (execute)

[這篇](http://s2.naes.tn.edu.tw/~kv/file.htm)對檔案權限說明的很清楚， umask 指令的功能是用來“限定”每一個新增的檔案、目錄的基本使用權限（permission）。當新增檔案時，會形成一個預設的使用者，每個使用者接有一個 umask 的設定值，例如 jenkins 使用者建立的檔案，如果不去修改，他會是 0002 表示只有自己可以修改，如果我們把他改成 0022，此時在相同群組下的使用者接能修改。

```
umask （顯示設定值）
umask nnn （設定umask，設定值為000~777的整數）
```

權限對照如下，他跟 chmod 有點不同。

![](/blog/images/pasted-19.png)

加入使用者，可新增於

```
$ vim ~./bashrc
```

```
umask 022
```

加入所有使用者

```
$ vim /etc/bash.bashrc
```

```
umask 022
```


# Network

* ping port 

```
nc -zv localhost 5432
```

# 加密 pdf 文件

主要可以區分幾個方式

* 編輯需要密碼
* 查看需要密碼

```bash
# 編輯需要密碼
pdftk [要加密的 pdf 文件] output [加密的 pdf 文件儲存位置] owner_pw [passwoed]
# 查看需要密碼
pdftk [要加密的 pdf 文件] output [加密的 pdf 文件儲存位置] user_pw [passwoed]
```

ref. [Is there a tool that can add a password to a PDF file?](https://askubuntu.com/questions/258848/is-there-a-tool-that-can-add-a-password-to-a-pdf-file)