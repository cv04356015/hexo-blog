title: 在 R 執行 Pyhon 語法
author: Hsien Ching Lo
tags:
  - R
categories: []
date: 2018-03-31 11:05:00
---
# Summary

rPython 無法運行 Anaconda 或是 virsualenv 上方的 Python 版本

# rPython installed

預設為 python2.7

```
sudo su -c "R -e \"install.packages('rPython', repos='https://cran.rstudio.com/')\""
sudo su -c "R -e \"remove.packages('rPython')\""
```

輸入指令 python 回傳版本

```
python.exec("import sys; print(sys.version[:3])")
```

```
python2.7
```

如果要安裝 python3 的版本

修改須注意已知兩個環境下無法安裝(2018-03-01)

* Anaconda 
* virtualenv

更換完環境在安裝 rPython 時會出現錯誤

configure 為 rPython 安裝設定檔

可至 [github](https://github.com/cran/rPython) 下載

rPython 以 C++ 編寫，需透過 gcc 執行命令

其中需新增 RPYTHON_PYTHON_VERSION 變數以供 R 讀取(包含其他變數皆會以這個變數作為依據)

詳細需求參考 configure 檔

```
sudo su
vim /etc/environment
```

add

```
RPYTHON_PYTHON_VERSION=3
```

如果無反應可能是 pip 尚未安裝

python 環境下需要看到以下幾個檔案，以 python3.5 為例

* python3.5
* python3.5m
* python3.5-config
* python3.5m-config


