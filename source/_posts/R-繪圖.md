title: R 繪圖
author: Hsien Ching Lo
tags:
  - R
categories: []
date: 2018-04-02 17:32:00
---


# Bump Chart

A Bump Chart is a special form of a line plot. This kind of plot is designed for exploring changes in rank over time. The focus here is usually on comparing the position or performance of multiple observations with respect to each other rather than the actual values itself.[reference](https://dominikkoch.github.io/Bump-Chart/)

![](\images\pasted-4.png)
