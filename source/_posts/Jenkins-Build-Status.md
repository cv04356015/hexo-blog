title: Jenkins Build Status
author: Hsien Ching Lo
date: 2018-06-01 08:07:17
tags:
---

# Jenkins 建立狀態監控

![](/blog/images/pasted-26.png)

管理 Jenkins -> 管理外掛程式 -> 可用的 -> embeddable-build-status -> 直接安裝

* 設定外部的人可以不登入直接看

管理 Jenkins -> 設定全域安全性 -> 授權 -> 矩陣型安全性 ->

```
Anonymous Users (只勾選 ViewStatus)
Authenticated Users (全勾)
```

專案 -> Embeddable Build Status -> 下拉選取 Markdown / HTML

```
<a href='http://192.168.171.247:8080/job/MT_MAIL/'><img src='http://192.168.171.247:8080/buildStatus/icon?job=MT_MAIL'></a>
```

貼在你想要放置的網頁上即可